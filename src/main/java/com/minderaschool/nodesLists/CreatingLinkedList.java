package com.minderaschool.nodesLists;

public class CreatingLinkedList {
    public NodeClass firstNode = null;
    public NodeClass lastNode = null;
    private int count = 0;

    public static void main(String[] args) {
        CreatingLinkedList list = new CreatingLinkedList();
        list.addNode("N0");
        list.addNode("N1");
        list.addNode("N2");
        list.print();
        System.out.println();
        list.remove(3);
        list.print();
        System.out.println("Getter: " + list.get(0));

        //Com isto construimos o metodo que cria LinkedList list = new LinkedList();
        //list.add("Aa")

    }

    public void addNode(String newtext) {
        if (firstNode == null) {
            firstNode = new NodeClass(newtext);
            count++;
            return;
        }

        NodeClass newNode = firstNode;

        // Este ciclo vai utilizar a referencia ".next" para percorrer os nodes enquanto esta for diferente de "null".

        for (; newNode.next != null; ) { // while(newNode.next != null){ }
            newNode = newNode.next;
        }
        newNode.next = new NodeClass(newtext);
        count++;

        //Em vez do ciclo while tb podemos usar:

        //lastNode.next = new NodeClass(newtext);
        //lastNode = lastNode.next;

        //Assim o nosso "lastNode" vai ser sempre o ultimo inserido.

    }

    public String get(int index) {

        if (index < 0 || index >= count) {
            return "Invalid index.";
        }
        NodeClass node = firstNode;

        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        if (index == 0) {
            return firstNode.text;
        }
        return node.text;
    }

    public void remove(int index) {

        if (index < 0 || index >= count) {
            System.out.println("Invalid index.");
            return;
        }
        NodeClass node = firstNode;

        for (int i = 0; i < index - 1; i++) { // while(newNode.next != null){ }
            node = node.next;
        }
        if (index == 0) {
            firstNode = node.next;
        } else {
            node.next = node.next.next;
        }
    }

    public void print() {
        NodeClass newNode = firstNode;

        do {
            System.out.println(newNode.text);
            newNode = newNode.next;
        } while (newNode != null);

    }
}
