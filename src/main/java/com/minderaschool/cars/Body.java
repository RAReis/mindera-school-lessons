package com.minderaschool.cars;

public class Body {

    private Door[] doors;
    //private boolean opened;

    public Body(int numDoors) {
        doors = new Door[numDoors];
        for (int i=0; i < doors.length; i++){
            doors[i] = new Door();
        }
    }

    public void open(int door){
        System.out.println("Opening door "+door);
        doors[door].open();
    }
    public void close(int door){
        System.out.println("Closing door "+door);
        doors[door].close();
    }
    public boolean isOpen(int door){
        System.out.println("Cheking if door "+door+" is open...");
        return doors[door].isOpened();

    }
}
