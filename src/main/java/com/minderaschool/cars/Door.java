package com.minderaschool.cars;

import com.minderaschool.utils.Logger;

public class Door {
    private boolean opened;
    private static final Logger logger = new Logger("Door");

    public boolean isOpened() {
        return opened;
    }

    public void open(){
        this.opened = true;
        logger.info("Door opened");

    }
    public void close(){
        logger.info("Door closed");
        this.opened = false;
    }
}
