package com.minderaschool.cars;

import com.minderaschool.utils.Logger;

public class Engine {
    private boolean started;
    private static final Logger logger = new Logger("Engine");


    public void start(){
        this.started = true;
        logger.debug("boolean started: " + this.started); // utilizamos debug para saber o valor do boolean
    }

    public void stop(){
        this.started = false;
    }
    public boolean isStarted(){
        return started;
    }
}
