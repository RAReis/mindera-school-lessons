package com.minderaschool.cars;

import com.minderaschool.utils.Logger;

public class Car {
    private static final Logger logger = new Logger("Car");
    private Engine engine;
    private Tire[] tire;
    private Body body;

    public Car(Engine engine, Tire[] tire, Body body) {
        this.engine = engine;
        this.tire = tire;
        this.body = body;

    }

    public void move() {
        if (engine.isStarted() == true) {
            for (int i = 0; i < tire.length; i++) {
                tire[i].rotate();
            }
        } else {
            logger.info("Starting engine...");
            engine.start();
        }
        logger.info("Moving...");
    }

    public void openDoor(int door) {
        if (body.isOpen(door) == false) {
            body.open(door);
        } else {
            logger.error("Door is already opened.");
        }
    }

    public void closeDoor(int door) {
        if(body.isOpen(door) == true){
        body.close(door);
        }else {
            logger.error("Door is already closed.");
        }
    }
    public void stop(){
        if(engine.isStarted() == true){
            engine.stop();
        }
        logger.info("Stoping car...");
    }

}

