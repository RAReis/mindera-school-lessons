package com.minderaschool.exercicioaGabriel.threads;

import java.util.HashMap;
import java.util.Map;

public class Fibonacci {

    Map<Long, Long> memoization;

    public Fibonacci() {
        this.memoization = new HashMap<>();
    }

    Long fibonacci(Long n) throws InterruptedException {

        if (n < 0) {
            throw new IllegalArgumentException();
        }
        if (n <= 1) {
            memoization.put(n, 1L);
            //put(n, 1L);
        }
        if (!memoization.containsKey(n)) {
            if (n <= 10) {
                FibCalc f1 = new FibCalc(n);
                Thread t1 = new Thread(f1);
                t1.start();
                t1.join();
                memoization.put(n, f1.getFibResult());

            } else {
                FibCalc f1 = new FibCalc(n - 1);
                FibCalc f2 = new FibCalc(n - 2);
                Thread t1 = new Thread(f1);
                Thread t2 = new Thread(f2);
                t1.start();
                t1.join();
                t2.start();
                t2.join();
                long result = f1.getFibResult() + f2.getFibResult();
                memoization.put(n, result);
                //put(n, result);
            }

        }
        return memoization.get(n);
    }

    class FibCalc implements Runnable {

        private Long n;
        private long fibResult;

        public FibCalc(Long n) {
            this.n = n;
        }

        @Override
        public void run() {
            if (n < 0) {
                throw new IllegalArgumentException();
            }
            if (n <= 1) {
                memoization.put(n, 1L);
                //put(n, 1L);
            }
            if (!memoization.containsKey(n)) {
                try {
                    fibResult = fibonacci(n - 1) + fibonacci(n - 2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                memoization.put(n, fibResult);
                //put(n, fibResult);
            }
            fibResult = memoization.get(n);
        }

        public long getFibResult() {
            return fibResult;
        }
    }

    public synchronized void put(Long k, Long v) {
        memoization.put(k, v);
    }
}

