package com.minderaschool.exercicioaGabriel;

public class CompareChar{
    public static void main(String[] args) {

        String[] arr = {"flower", "flow", "flight"};
        String[] arr1 = {"dog", "racecar", "car"};
        String[] arr2 = {"float", "flop", "floppy", "flow"};
        String[] arr3 = {"a", "a", "ab", "a"};

        System.out.println(commonPrefix(arr2));
    }

    public static String commonPrefix(String[] arr) {
        if (arr.length == 0) {
            return "";
        }

        String result = "";

        int count = 0;
        int i = 0;

        while (isCountValid(arr, i, count) && charsAreEqual(arr[i], arr[i+1], count)) {

            i++;

            if (i >= arr.length - 1) {
                i = 0;
                result += arr[i].charAt(count);
                count++;
            }
        }

        return result;
    }

    public static boolean isCountValid(String[] arr, int i, int count) {

        boolean emptyArr = arr[i].isEmpty() && arr[i + 1].isEmpty();
        boolean smallerThanCount = arr[i].length() > count && arr[i + 1].length() > count;

        return !emptyArr && smallerThanCount;
    }

    public static boolean charsAreEqual(String actual, String next, int count){
        return actual.charAt(count) == next.charAt(count);
    }
}
