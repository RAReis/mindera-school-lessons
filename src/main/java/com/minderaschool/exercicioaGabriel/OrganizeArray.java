package com.minderaschool.exercicioaGabriel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class OrganizeArray {

    public static void main(String[] args) {

        List<Integer> initialList = new ArrayList<>();
        initialList.add(8);
        initialList.add(9);
        initialList.add(2);
        initialList.add(3);
        initialList.add(7);
        initialList.add(5);
        initialList.add(13);
        initialList.add(9);
        initialList.add(2);
        initialList.add(8);

        List<Integer> compList = new ArrayList<>();
        compList.add(9);
        compList.add(3);
        compList.add(13);
        compList.add(5);


        initialList = relativeSorting2(initialList, compList);


        for (Integer i : initialList) {
            System.out.print(i + " - ");
        }
    }

    private static List<Integer> relativeSorting2(List<Integer> a, List<Integer> o) {
        return a.stream()
                .sorted(relativeSortingComparator(o))
                .collect(toList());
    }

    private static Comparator<Integer> relativeSortingComparator(List<Integer> comp) {
        return (o1, o2) -> {

            if (comp.contains(o1)) {
                if (comp.contains(o2)) {

                    if (comp.indexOf(o1) == comp.indexOf(o2)) {
                        return 0;
                    } else if (comp.indexOf(o1) < comp.indexOf(o2)) {
                        return -1;
                    } else {
                        return 1;
                    }
                } else {
                    return -1;
                }

            } else if (comp.contains(o2)) {
                return 1;
            } else {

                if (o1 == o2) {
                    return 0;
                } else if (o1 < o2) {
                    return -1;
                } else {
                    return 1;
                }
            }

        };
    }
}
