package com.minderaschool.exercicioaGabriel.ChocolateDistribution;

import java.util.Arrays;

public class main {
    public static void main(String[] args) {
        int[] arr = {3, 4, 1, 9, 56, 7, 9, 12};
        int students = 5;

        System.out.println(chocolateDistribution(arr, students));
    }

    private static int chocolateDistribution(int[] a, int m) {
        int result = Integer.MAX_VALUE;
        int startIndex = 0;
        int endIndex = (startIndex + m) - 1;
        Arrays.sort(a);

        while (endIndex <= a.length-1) {

            if (a[endIndex] - a[startIndex] < result) {
                result = a[endIndex] - a[startIndex];
            }
            endIndex++;
            startIndex++;
        }

        return result;
    }


}
    /*
    Dado X pacotes de chocolates de tamanhos variados, dividam-nos por Y alunos
    para que a diferença de chocolates seja o mais justo possível
    (a diferença entre os alunos com mais chocolates e com menos chocolates seja menor).

    private static int chocolateDistribution(int[] a, int m)

        a: 3 4 1 9 56 7 9 12
        m: 5
        result: 6 (3, 4, 7, 9, 9) (9-3 = 6)
        */