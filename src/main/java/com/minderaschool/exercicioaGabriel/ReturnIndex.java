package com.minderaschool.exercicioaGabriel;

public class ReturnIndex {
    public static void main(String[] args) {
        int[] arr = {2, 3, 7, 11, 15};
        int target = 9;
        int[] result = indexOfSum(arr, target);
        printArr(result);
    }

    public static int[] indexOfSum(int[] arr, int target) {
        int[] indexArr = new int[]{-1, -1};
        int j = arr.length-1;
        int i = 0;

        do{
            if(arr[i] + arr[j] > target){
                j--;
            } else if(arr[i] - arr[j] < target){
                i++;
            } else {
                break;
            }

        }while(target != arr[i] + arr[j]);

        indexArr[0] = i;
        indexArr[1] = j;
        return indexArr;
    }

    public static void printArr(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}
