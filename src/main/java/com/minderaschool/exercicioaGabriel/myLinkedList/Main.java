package com.minderaschool.exercicioaGabriel.myLinkedList;

public class Main {
    public static void main(String[] args) {
        MyLinkedList linkedList = new MyLinkedList();
        linkedList.print();
        linkedList.addAtHead(1);
        linkedList.addAtTail(3);
        linkedList.addAtIndex(1, 2);  // linked list becomes 1->2->3
        linkedList.print();
        System.out.printf("%d is the value of node in index %d. \n",linkedList.get(1),1);
        linkedList.deleteAtIndex(1);  // now the linked list is 1->3
        linkedList.print();
        linkedList.get(1);            // returns 3

    }
}
