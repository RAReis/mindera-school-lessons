package com.minderaschool.exercicioaGabriel.myLinkedList;

public class MyLinkedList {
    private Node head;
    private Node tail;
    private int size = 0;

    public Node getHead(){
        return head;
    }

    public int get(int index) {

        if (index < 0 || index >= size || head == null) {
            throw new ArrayIndexOutOfBoundsException(index);
        }

        if (index == 0) {
            return head.value;
        }

        Node nodeToSearch = head;

        for (int i = 1; i <= index; i++) {
            nodeToSearch = nodeToSearch.nextNode;
        }
        return nodeToSearch.value;
    }

    public int getSize() {
        return size;
    }

    public Node getTail() {
        return tail;
    }

    public void addAtHead(int val) {
        if (head == null) {
            head = new Node(val);
            tail = head;
            size++;
            return;
        }

        Node newNode = new Node(val);
        newNode.nextNode = head;
        head = newNode;
        size++;
    }

    public void addAtTail(int val) {
        Node newNode = new Node(val);

        tail.nextNode = newNode;
        tail = newNode;

        size++;
    }

    public void addAtIndex(int index, int val) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException();
        }

        if(index == 0){
            addAtHead(val);
            size++;
            return;
        }

        if (index == size) {
            addAtTail(val);
            size++;
            return;
        }

        Node newNode = new Node(val);
        Node current = head;

        for (int i = 1; i <= index-1; i++) {
            current = current.nextNode;
        }
        newNode.nextNode = current.nextNode;
        current.nextNode = newNode;
        size++;

    }

    public void deleteAtIndex(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }

        Node current = head;

        for (int i = 0; i < index-1; i++) {
            current = current.nextNode;
        }

        if (index == 0) {
            head = current.nextNode;
        } else {
            current.nextNode = current.nextNode.nextNode;
        }
        size--;

    }

    public void print() {

        if (head == null) {
            System.out.println("LinkedList is empty.");
            return;
        }

        Node newNode = head;

        do {
            System.out.print(newNode.value+ " ");
            newNode = newNode.nextNode;
        } while (newNode != null);
        System.out.println();

    }

    public class Node {
        private int value;
        private Node nextNode = null;

        public Node(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
