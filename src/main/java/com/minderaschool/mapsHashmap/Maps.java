package com.minderaschool.mapsHashmap;


import java.util.*;

public class Maps {
    public static void main(String[] args) {
        //ex1();
        ex2();


    }

    public static void ex1() {
        ArrayList<String> list = new ArrayList() {{ // Podemos utilizar esta sintax em vez de repetir o comando
            add("cao");                             //  lista.add(" ");
            add("gato");
            add("passaro");
            add("banana");
            add("morango");
            add("morango");
        }};

        //HashMap<String, Integer> map = new HashMap<>(); // cria map sem ordem
        //Hashtable<String, Integer> map = new Hashtable<>(); // igual ao HashMap mas nao aceita nulls
        //LinkedHashMap<String, Integer> map = new LinkedHashMap<>(); // cria map e ordena pela ordem em que foram adicionadas as keys.
        TreeMap<String, Integer> map = new TreeMap<>(); // ordena por ordem alfabetica


        for (int i = 0; i < list.size(); i++) {
            String key = list.get(i);
            if (map.containsKey(key)) {
                Integer x = map.get(key);
                x = x + 1;
                map.put(key, x);

            } else {
                map.put(key, 1);
            }
        }
        for (Map.Entry entry : map.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        System.out.println(map.keySet()); // mostra todas as keys nao repetidas no map

    }


    public static void ex2() {
        HashMap<Integer, String> map2 = new HashMap<>();

        map2.put(4, "a");
        map2.put(5, "b");
        map2.put(6, "c");
        map2.put(7, "d");
        map2.put(8, "a");
        map2.put(9, "a");

        Map.Entry<Integer, String> entry;
        Iterator<Map.Entry<Integer, String>> it;

        it = map2.entrySet().iterator();

        while (it.hasNext()) {
            entry = it.next();
            System.out.println(entry.getKey() + "=" + entry.getValue());
            if (map2.values().contains(entry.getValue())) {
                System.out.println("contain");
            }
            System.out.println(map2);

        }


    }
}
