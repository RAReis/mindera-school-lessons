package com.minderaschool.mapsHashmap;

public class MainContacts {
    public static void main(String[] args) {

        Contact c1 = new Contact("e-mail", "qwery@gmail.com");
        Contact c2 = new Contact("phone", "939732732");
        Contact c3 = new Contact("phone", "939632632");
        Contact c4 = new Contact("e-mail", "qwerty@hotmail.com");

        Person person1 = new Person("ANDRE", c1);
        Person person2 = new Person("ANDRE", c2);
        print(person1);


    }

    public static void print(Person person) {
            System.out.print(person.getName() + " ");
            for (Contact con : person.getContacts()) {
                System.out.println(con.getType() + ":" + " " + con.getValue());

            }
        }
    }


