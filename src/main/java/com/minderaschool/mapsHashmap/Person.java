package com.minderaschool.mapsHashmap;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private String name;
    private List<Contact> contacts = new ArrayList<Contact>();

    public Person(String name, List<Contact> contacts) {
        this.name = name;
        this.contacts = contacts;
    }

    public Person(String name, Contact contact) {
        this.name = name;
        this.contacts.add(contact);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public void addContact(Contact contact){
        this.contacts.add(contact);
    }
}

