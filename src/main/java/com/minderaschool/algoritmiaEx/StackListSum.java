package com.minderaschool.algoritmiaEx;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class StackListSum {

    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<Integer>() {{
            add(1);
            add(2);
            add(3);
        }};

        List<Integer> list2 = new ArrayList<Integer>() {{
            add(4);
            add(5);
            add(6);
        }};

        stackSum(list1, list2);
    }

    public static void stackSum(List<Integer> list1, List<Integer> list2){

        Stack<Integer> stack = new Stack<Integer>();
        for(int i = list1.size()-1; i >= 0; i--) {
            stack.push(list1.get(i) + list2.get(i));
        }
        for(int i = 0; i < list1.size(); i++){
            System.out.print(stack.pop()+ " ");
        }
    }
}
