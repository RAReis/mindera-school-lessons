package com.minderaschool.algoritmiaEx;

public class CountVowels {
    public static void main(String[] args) {
        vowelsCheck("asesisos");
    }

    public static void vowelsCheck(String str){
        str = str.toLowerCase();
        char[] vowelsArr = {'a','e','i','o','u'};
        char[] strArr = str.toCharArray();
        int vowel = 0;
        int cons = 0;
        for(int i = 0; i < strArr.length; i++){
            for(int j = 0; j < vowelsArr.length; j++){
                if(strArr[i] == vowelsArr[j]){
                    vowel++;
                    break;
                }
            }
            cons++;
        }
        System.out.println("vowels: "+vowel+" consonants: "+cons/2);
    }
}
