package com.minderaschool.algoritmiaEx;
import java.util.LinkedHashMap;
import java.util.Map;

public class PrintNonRepeated {
    public static void main(String[] args) {
        String string1 = "simplest";
        String string2 = "absdfghija";

        firstUnicChar(string1);
    }

    public static void firstUnicChar(String string) {

        char[] charArr = string.toCharArray();
        LinkedHashMap<Character, Integer> map = new LinkedHashMap<>();
        for(int i = 0; i < charArr.length; i++){
            if(map.containsKey(charArr[i])){
                map.replace(charArr[i],map.get(charArr[i])+1);
            }else{
            map.put(charArr[i], 1);
        }}

        for(Map.Entry entry : map.entrySet()){
            System.out.println(entry.getKey()+ " " + entry.getValue());

            if(entry.getValue().equals(1)){
                System.out.println("->" + entry.getKey());
                break;
            }
        }
    }


}
