package com.minderaschool.algoritmiaEx;

import java.util.concurrent.ThreadLocalRandom;

public class MissingNumber {
    /**
     * Main is su....
     */
    public static void main(String[] args) {
        int count = 1;
        Integer[] arr = new Integer[99];

        /*for(int i = 0; i < arr.length; i++){
            if(count == 50) {
                count++;
            }
                arr[i] = count;
            count++;
        }*/

        int[] random = ThreadLocalRandom.current().ints(1, 101).distinct().limit(99).toArray();


        for (int i : random) {
            System.out.println(count + "--" + i);
            count++;
        }

        System.out.println();

        missingNum(random);

    }

    public static void missingNum(int[] arr) {
        int missingNum = 0;
        int sumOfArr = 5050; // soma de todos os numeros no array
        int sumOfMiss = 0;

        for(int i = 0; i < arr.length; i++){
             sumOfArr = sumOfArr - arr[i];
        }
        missingNum = ((sumOfArr-sumOfMiss));
        System.out.println("Missing number: " + sumOfArr);
    }
}
