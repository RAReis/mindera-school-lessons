package com.minderaschool.algoritmiaEx;

import java.util.Arrays;

public class CheckIfAnagram {
    public static void main(String[] args) {
        String Listen = "Listen";
        String Silent = "Silent";
        String nope = "nope";

        checkAnag(Listen, Silent);
        checkAnag(Listen, nope);
    }

    public static void checkAnag(String string1, String string2) {

        if (string1.length() != string2.length()) {
            System.out.println("The words are not anagrams, they are not the same size.");
            return;
        } else {

            string1 = string1.toLowerCase();
            string2 = string2.toLowerCase();
            char[] arr1 = string1.toCharArray();
            char[] arr2 = string2.toCharArray();
            Arrays.sort(arr1);
            Arrays.sort(arr2);

            for (int i = 0; i < arr1.length; i++) {
                if (arr1[i] != arr2[i]) {
                    System.out.println("Words are not anagrams, they have different chars.");
                    return;
                }
            }
            System.out.println("Words are anagrams.");

            /**Outra solução seria:

                if(Arrays.equals(arr1, arr2)){
                System.out.println("Words are anagrams.");
                } else {
                System.out.println("Words are not anagrams.");
                }

             **/

        }


    }

}
