package com.minderaschool.algoritmiaEx;

public class Palindrome {
    public static void main(String[] args) {
        String string1 = "asdsa";
        String string2 = "QWEewq";
        String string3 = "zxcvasz";

        PalindromeCheck(string1);
        PalindromeCheck(string2);
        PalindromeCheck(string3);
    }

    public static void PalindromeCheck(String str){
        str = str.toLowerCase();
        char[] arr = str.toCharArray();
        int j = arr.length-1;

        for(int i = 0; i < (arr.length/2); i++){
            if(arr[i] != arr[j]){
                System.out.println("Word is not a palindrome.");
                return;
            } else {
                j--;
            }
        }
        System.out.println("Word is a palindrome.");
    }
}
