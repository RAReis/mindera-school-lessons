package com.minderaschool.files;

import java.io.*;
import java.util.Scanner;

public class FilesEx {
    public static void main(String[] args) {
        Scanner scanner = new Scanner( System.in);
        String input;

        File arquivo = new File("lessons/src/main/resources/arquivo.txt");

        try {
           // arquivo.createNewFile(); // criar ficheiro
            FileReader ler = new FileReader(arquivo);

            FileWriter fileWriter = new FileWriter(arquivo);
            BufferedWriter escrever = new BufferedWriter(fileWriter);
            //escrever.write("escrevendo");
            do{
                System.out.print("Write something or 0 to exit: ");
                 input = scanner.next();
                 escrever.write(input);
                 escrever.newLine(); // passa para a proxima linha, algo como "println"

            }while(!input.equals("0"));
            escrever.close();
            fileWriter.close();

            BufferedReader lerB = new BufferedReader(ler);
            String linha = lerB.readLine();

            while(linha != null){
                System.out.println(linha);
                linha = lerB.readLine();
            }

            File fil = new File("lessons/src/main");

            File fi [] = fil.listFiles();
            for(int i=0; i<fi.length; i++){
                System.out.println(fi[i]);
            }


        }catch (IOException ex) {
        }


        /*Scanner input = new Scanner(System.in);
        System.out.println("Choose size of array: ");
        int size = input.nextInt();
        int maximo = Integer.MIN_VALUE;
        int maximo2 = Integer.MIN_VALUE;
        int[] intArray = new int[size];

        System.out.println("Enter " + size + " numbers");
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = input.nextInt();
            if (intArray[i] > maximo) {
                maximo = intArray[i];
            }
        }
        for (int i = 0; i < intArray.length; i++) {
            if (intArray[i] > maximo2 && intArray[i] < maximo) {
                maximo2 = intArray[i];
            }
        }
        System.out.println("Max Value: " + maximo);
        System.out.println("Max Value 2: " + maximo2);*/


    }
}