package com.minderaschool.inheritance;

public class Figure {
    String iAm = "I'm a Figure";
    double figureArea = 69.69;
    public double height;
    public double length;
    public double side;
    public double radius;
    public double base;

    public double area(){
    return figureArea;
    }

    public void whatAmI(){
        System.out.println(iAm);
    }
}
