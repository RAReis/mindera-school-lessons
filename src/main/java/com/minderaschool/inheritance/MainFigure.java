package com.minderaschool.inheritance;

import java.util.ArrayList;

public class MainFigure {
    public static void main(String[]args){


        Figure figure = new Figure();
        System.out.println("Figure area: "+figure.area());
        figure.whatAmI();
        System.out.println();

        Rectangle rectangle = new Rectangle(2, 4);
        System.out.println("Rectangle area: "+rectangle.area());
        rectangle.whatAmI();
        System.out.println();

        Circle circle = new Circle(2.5);
        System.out.println("Circle area: "+circle.area());
        circle.whatAmI();
        System.out.println();

        Triangle triangle = new Triangle(3, 5);
        System.out.println("Triangle area: "+triangle.area());
        triangle.whatAmI();
        System.out.println();

        Square square = new Square(2);
        System.out.println("Square area: "+square.area());
        square.whatAmI();
        System.out.println();

        // Usando listas:
        ArrayList<Figure> list = new ArrayList<Figure>();

        // Adicionamos "Circle" à lista
        list.add(new Circle(4));
        // Chamamos o metodo "whatAmI" que pertence ao circulo, estando este na posição '0' da nossa lista
        list.get(0).whatAmI();
        System.out.println(list.get(0).area());

        list.add(new Rectangle(2,4));
        list.add(new Square(2));
        list.add(new Triangle(3,5));
        list.add(new Figure());

        // Criamos um ciclo "for" para chamar todos os objetos na lista e os seus metodos
        for(int i = 0; i < list.size(); i++){
            list.get(i).whatAmI();
            System.out.println(list.get(i).area());
        }


    }
}
