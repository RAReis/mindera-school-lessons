package com.minderaschool.inheritance;

public class Triangle extends Figure{

    public Triangle(double base, double height){
        super.base = base;
        super.height = height;
        super.figureArea = (base*height)/2;
        super.iAm = "I'm a Triangle";
    }
    public double area(){
        return figureArea;
    }

    public void whatAmI(){
        System.out.println(iAm);
    }
}
