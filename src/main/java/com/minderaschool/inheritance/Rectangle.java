package com.minderaschool.inheritance;

public class Rectangle extends Figure {

    public Rectangle(){}

    public Rectangle(double height, double length){
        super.height = height;
        super.length = length;
        super.figureArea = height*length;
        super.iAm = "I'm a Rectangle";
    }

    public double area(){
        return figureArea;
    }

    public void whatAmI(){
        System.out.println(iAm);
    }
}
