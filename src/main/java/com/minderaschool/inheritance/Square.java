package com.minderaschool.inheritance;

public class Square extends Rectangle {

    public Square(double side){
        super.side = side;
        super.figureArea = side*side;
        super.iAm = "I'm a Square";
    }
    public double area(){
        return figureArea;
    }

    public void whatAmI(){
        System.out.println(iAm);
    }
}
