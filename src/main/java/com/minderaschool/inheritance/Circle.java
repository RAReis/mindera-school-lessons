package com.minderaschool.inheritance;

public class Circle extends Figure {


    public Circle(double radius){
        super.radius = radius;
        super.figureArea = Math.PI*(radius*radius);
        super.iAm = "I'm a Circle";
    }

    public double area(){
        return figureArea;
    }

    public void whatAmI(){
        System.out.println(iAm);
    }
}
