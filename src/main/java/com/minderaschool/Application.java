package com.minderaschool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("name");
        stringList.add("names");
        stringList.add("cat");
        stringList.add("cats");
        stringList.add("dog");
        stringList.add("dogs");

        System.out.println(stringList.toString());
        List<Integer> list1 = Arrays.asList(1, 4, 8, 9, 11, 15, 17, 28, 41, 59);
        List<Integer> list2 = Arrays.asList(4, 7, 11, 17, 19, 20, 23, 28, 37, 59, 81);

        // Tambem podemos usar ciclos "for" para a adição dos valores de um array para uma lista

        /*int[] arr1 = {1, 4, 8, 9, 11, 15, 17, 28, 41, 59};
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        ArrayList<Integer> list2 = new ArrayList<Integer>();
        int[] arr2 = {4, 7, 11, 17, 19, 20, 23, 28, 37, 59, 81};

        for(int i = 0; i<arr1.length; i++) {
            int num1 = arr1[i];
            list1.add(num1);
        }
        for(int i = 0; i<arr2.length; i++) {
            int num2 = arr2[i];
            list2.add(num2);
        }*/

        System.out.println(list1.toString());
        System.out.println(list2.toString());

        ArrayList<Integer> result = intersect(list1, list2);
        System.out.println("main" + result.toString());

        ArrayList<Integer> reversed = reverse(result);
        System.out.println("reversed" + reversed);

        System.out.println(stringList.toString());
        ArrayList<String> capitalized = capitalizePlurals(stringList);
        System.out.println(capitalized.toString());
        ArrayList<String> removed = removePlurals(stringList);
        System.out.println(removed.toString());

    }

    public static ArrayList<Integer> intersect (List < Integer > list1, List < Integer > list2){
        //usamos "List" em vez de "ArrayList"
        ArrayList<Integer> intersectList = new ArrayList<>();

        for (int i = 0; i < list1.size(); i++) {
            for (int j = 0; j < list2.size(); j++) {
                if (list1.get(i) == list2.get(j)) {
                    intersectList.add(list2.get(j));
                }
            }
        }
        return intersectList;
    }

    public static ArrayList<Integer> reverse (ArrayList < Integer > result) {

        ArrayList<Integer> reverseList = new ArrayList<>();
        for (int i = result.size() - 1; i > 0; i--) {
            reverseList.add(result.get(i));

        }
        return reverseList;

    }
    public static ArrayList<String> capitalizePlurals (ArrayList<String> stringList){
        for(int i=0; i< stringList.size(); i++){
            String string = stringList.get(i);
            if(string.charAt(string.length()-1) == 's'){
                stringList.set(i,string.toUpperCase());
            }

        }

        return stringList;
    }
    public static ArrayList<String> removePlurals (ArrayList<String> stringList) {
        for (int i = 0; i < stringList.size(); i++) {
            String string = stringList.get(i);
            if (string.charAt(string.length() - 1) == 's' || string.charAt(string.length() - 1) == 'S') {
                stringList.remove(i);
            }
        }

        return stringList;
    }
}



