package com.minderaschool.enumMonths;

public class EnumMonths {
    public static void main(String[] args) {
        Months months = Months.JANUARY;
        Months weirdMonth = Months.FEBRUARY;

        switch (months) {
            case JANUARY:
            case MARCH:
            case MAY:
            case JULY:
            case AUGUST:
            case DECEMBER:
                System.out.println("This month has 31 days.");
                break;
            case APRIL:
            case JUNE:
            case SEPTEMBER:
            case OCTOBER:
            case NOVEMBER:
                System.out.println("This month has 30 days.");
                break;
            case FEBRUARY:
                System.out.println("This month is a weirdo, it has 28 or 29 days!");
                break;
        }

        System.out.println("Number of days: " + months.getNumberOfDays());
        System.out.println("Ordinal: " + months.ordinal());

        System.out.println("Number of days: " + weirdMonth.getNumberOfDays());
        System.out.println("Ordinal: " + weirdMonth.ordinal());

    }

}
