package com.minderaschool.utils;

public class Converter {

    public static int romanToDecimal(String roman) {

        char[] charArr = roman.toCharArray();
        int num = 0;
        int previousnum = 0;
        int decimal = 0;
        for (int i = charArr.length - 1; i >= 0; i--) {
            char charRoman = charArr[i];
            charRoman = Character.toUpperCase(charRoman);
            switch (charRoman) {

                case 'I':
                    previousnum = num;
                    num = 1;
                    break;
                case 'V':
                    previousnum = num;
                    num = 5;
                    break;
                case 'X':
                    previousnum = num;
                    num = 10;
                    break;
                case 'L':
                    previousnum = num;
                    num = 50;
                    break;
                case 'C':
                    previousnum = num;
                    num = 100;
                    break;
                case 'D':
                    previousnum = num;
                    num = 500;
                    break;
                case 'M':
                    previousnum = num;
                    num = 1000;
                    break;
            }
            if (num < previousnum) {
                decimal = decimal - num;
            } else {
                decimal = decimal + num;
            }
        }
        return decimal;
    }

}


