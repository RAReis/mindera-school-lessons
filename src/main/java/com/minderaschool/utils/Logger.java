package com.minderaschool.utils;
// Usamos o logger para receber mensagens durante a execução do codigo
public class Logger {
    private String name;

    public Logger(String name) {
        this.name = name;
    }

    public void info(String msg) {
        System.out.println(name + " - INFO - " + msg);
    }

    public void error(String msg) {
        System.out.println(name + " - ERROR - " + msg);
    }

    public void debug(String msg) {
        System.out.println(name + " - DEBUG - "+ msg);
    }
}
