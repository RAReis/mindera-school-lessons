package com.minderaschool.exerciciosVasco.MyVersion;

import java.util.Map;

public class MyMetric {
    private String myName;
    private Double myValue;
    private String myType;
    private Map<String, String> myLabels;

    public MyMetric(String myName, Double myValue, String myType, Map<String, String> myLabels) {
        this.myName = myName;
        this.myValue = myValue;
        this.myType = myType;
        this.myLabels = myLabels;
    }

    public String getMyName() {
        return myName;
    }

    public Double getMyValue() {
        return myValue;
    }

    public String getMyType() {
        return myType;
    }

    public Map<String, String> getMyLabels() {
        return myLabels;
    }
}
