package com.minderaschool.exerciciosVasco.MyVersion;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyApp {
    public static List<MyMetric> lista = new ArrayList();
    public static  double sumValue = 0;

    public static void main(String[] args) {
        readFile();
        for(int i = 0; i < lista.size(); i++){
        System.out.println("Name: " + lista.get(i).getMyName()
                +" Value: " + lista.get(i).getMyValue()
                +" Type: " + lista.get(i).getMyType()
                +" Labels: " + lista.get(i).getMyLabels());

        }
         sumValue(lista);


    }

    public static void readFile(){
        BufferedReader reader;
        try{
            reader = new BufferedReader(new FileReader("metrics.txt"));
            String fileLine;
            while((fileLine = reader.readLine()) != null){
                splitLine(fileLine);
                //System.out.println(fileLine);
            }
        } catch(Exception e){
            System.out.println("Exception: " + e.toString());
        }
    }
    public static void splitLine(String line){
        Map<String, String> labels = new HashMap();
        String[] splitArr = line.split("\\|");
        //System.out.println(splitArr[0]);
        String type = splitArr[1];
        String[] splitFirst = splitArr[0].split(":");
        String name = splitFirst[0];

        // Transformando uma string em double:
        //https://www.journaldev.com/18392/java-convert-string-to-double
        Double value = Double.parseDouble(splitFirst[1]);

        // https://docs.oracle.com/javase/7/docs/api/java/lang/String.html#substring(int)
        splitArr[2] = splitArr[2].substring(1); // start removing everything before the index "1"

        String[] splitLastArr = splitArr[2].split(",");
        //System.out.println(splitLastArr[0]);
        for (String last : splitLastArr){
            String[] label = last.split(":");
            labels.put(label[0], label[1]);
        }
        lista.add(new MyMetric(name,value,type,labels));
        }

        public static void sumValue(List<MyMetric> lista){
            for(int i = 0; i < lista.size(); i++){
                Map<String, String> label = lista.get(i).getMyLabels();
                if(label.containsValue("210")){
                    sumValue += lista.get(i).getMyValue();
                }
            }
            System.out.printf("Door 210 was open %.0f times.\n",sumValue);
        }
    }


