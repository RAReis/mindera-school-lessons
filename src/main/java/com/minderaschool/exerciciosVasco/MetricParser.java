package com.minderaschool.exerciciosVasco;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class MetricParser {

    public static Collection<Metric> readAndParseMetrics(String filename) throws Exception {
        Path inputfile = Paths.get(filename);
        List<String> fileLines = Files.readAllLines(inputfile);

        List<Metric> metrics = new ArrayList<>(fileLines.size());

        System.out.printf("Read file with %d lines!!\n", fileLines.size());

        for(String line : fileLines) {
            //System.out.printf("DEBUG: parsing line \"%s\"\n", line);
            // break the line into all components split by 3
            String[] elements = line.split("\\|");
            if(elements.length != 3) {
                System.out.printf("BAD LINE, EXPECTED AT LEAST 3 ELEMENTS: %s\n", line);
                continue;
            }

            String nameAndValueString = elements[0];
            String type = elements[1];
            String labelsElements = elements[2];

            String[] nameAndValueArray = nameAndValueString.split(":");

            if(nameAndValueArray.length != 2 ) {
                System.out.printf("BAD LINE, EXPECTED AT LEAST 2 ELEMENTS (NAME AND VALUE): %s\n", nameAndValueString);
                continue;
            }
            String name = nameAndValueArray[0];

            double value = Double.parseDouble(nameAndValueArray[1]);


            // KEY1:VALUE1,NOME:ANDRE,PROFISSAO:ESTUDANTE
            String[] labelPairs = labelsElements.substring(1).split(",");

            // split gives
            // KEY1:VALUE1
            // NOME:ANDRE
            // PROFISSAO:ESTUDANTE

            Map<String, String> metricLabels = new HashMap<>();

            // for each labelPair which contains KEY1:VALUE1 ou NOME:ANDRE ou PROFISSAO:ESTUDANTE
            for(String labelPair : labelPairs) {
                int separatorPosition = labelPair.indexOf(":");
                String labelKey = labelPair.substring(0, separatorPosition);
                String labelValue = labelPair.substring(separatorPosition + 1, labelPair.length());

                metricLabels.put(labelKey, labelValue);
            }

            switch(type) {
                case "c":
                    metrics.add(new MetricCounter(name, value, metricLabels));
                    break;
                case "ms":
                    metrics.add(new MetricTimer(name, value, metricLabels));
                    break;
                case "g":
                    metrics.add(new MetricGauge(name, value, metricLabels));
                    break;
            }

        }

        return metrics;
    }

}