package com.minderaschool.exerciciosVasco;

import java.util.Map;

public class MetricTimer extends Metric {

    public MetricTimer(String name, double value, Map<String, String> labels) {
        super(name, value, labels);
    }
}
