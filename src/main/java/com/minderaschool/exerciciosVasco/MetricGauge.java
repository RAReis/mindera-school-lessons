package com.minderaschool.exerciciosVasco;

import java.util.Map;

class MetricGauge extends Metric {

    public MetricGauge(String name, double value, Map<String, String> labels) {
        super(name, value, labels);
    }

    public static class MetricsQuery {
    }
}