package com.minderaschool.exerciciosVasco;
import java.util.Map;

public class Metric {
    private String name;
    private double value;
    private Map<String, String> labels;

    public Metric(String name, double value, Map<String, String> labels) {
        this.name = name;
        this.value = value;
        this.labels = labels;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public String toString() {
        return String.format("Name: %s, %.2f", name, value);
    }
}