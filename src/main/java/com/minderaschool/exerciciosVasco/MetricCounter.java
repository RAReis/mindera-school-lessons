package com.minderaschool.exerciciosVasco;
import java.util.Map;

public class MetricCounter extends Metric {

    public MetricCounter(String name, double value, Map<String, String> labels) {
        super(name, value, labels);
    }
}