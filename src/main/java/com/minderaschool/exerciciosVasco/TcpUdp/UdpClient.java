import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class UdpClient { // no need for connection, the message is sent it doesn't matter if it was received
    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);

        DatagramSocket ds = new DatagramSocket();
        System.out.println("Write somethig...");
        String str = sc.nextLine();

        // We create a byte array because its one of the parameters that "DatagramPacket" will ask
        byte[] byteArrClient = str.getBytes();

        // "InetAddress" to get the IP parameter for "DatagramPacket"
        InetAddress ip = InetAddress.getLocalHost();
        DatagramPacket dp = new DatagramPacket(byteArrClient, byteArrClient.length, ip, 4444);

        // Output data
        ds.send(dp);


        // Accepting data from server
        byte[] byteArrServer = new byte[1024];
        DatagramPacket dp1 = new DatagramPacket(byteArrServer, byteArrServer.length);

        // Input data
        ds.receive(dp1);

        String str1 = new String(dp1.getData());
        System.out.println();
        System.out.println("Answer is " + str1);
        //System.exit(0);
        ds.close();
    }
}
