

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpServer {
    public static void main(String[] args) throws IOException {

        DatagramSocket ds = new DatagramSocket(4444);
        System.out.println("Server online!");

        byte[] byteArrServer = new byte[1024];

        DatagramPacket dp = new DatagramPacket(byteArrServer, byteArrServer.length);

        // Input data
        ds.receive(dp);


        // we use the offset parameter to choose the starting point of the array, and the next parameter to set the end off it
                //if we don't it will print all the 1024 slots, empty ones included
        String str = new String(dp.getData(), 0, dp.getLength()) + "\n Wow... So interesting...";

//        System.out.println("Got " + dp.getLength() + " bytes");
        System.out.println(dp.getAddress() + " : " + str);

        for(int i = 0; i < str.length(); i++) {
//            System.out.printf("str.charAt(%d): '%d'\n", i, (int) str.charAt(i));
        }


        byte[] byteArrClient = str.getBytes();
        InetAddress ip = InetAddress.getLocalHost();
        DatagramPacket dp1 = new DatagramPacket(byteArrClient,byteArrClient.length,ip,4444);

        //Output data
        ds.send(dp1);

    }
}
