import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class TcpServer {
    public static void main(String[] args) throws IOException {
        String string, temp;
        ServerSocket serSocket = new ServerSocket(50000);

        System.out.println("Server online!");
        Socket cliSocket = serSocket.accept();

        System.out.printf("<%s> Connected", cliSocket.getInetAddress());
        Scanner sc = new Scanner(cliSocket.getInputStream());
        string = sc.nextLine();

        System.out.println();
        System.out.println(string + "? ");

        // temp is the answer to the client
        temp = string + "? " + "Talk to the hand!!";
        PrintStream ps = new PrintStream(cliSocket.getOutputStream());
        ps.println(temp);

        System.out.println("end");


    }
}
