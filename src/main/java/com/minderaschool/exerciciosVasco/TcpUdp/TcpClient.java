import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class TcpClient {
    public static void main(String[] args) throws IOException {
        String string, temp;
        Scanner sc = new Scanner(System.in);
        Socket cliSocket = new Socket("127.0.0.1", 50000);


        System.out.println("Enter a string: ");

        // "string" stores the output sent by client
        string = sc.next();


        // "ps" will get the string sent by the client(Output)
        PrintStream ps = new PrintStream(cliSocket.getOutputStream());
        ps.println(string);

        // "sc1" will get the string coming from server(Input)
        Scanner sc1 = new Scanner((cliSocket.getInputStream()));
        // "temp" stores input coming from server
        temp = sc1.nextLine();
        System.out.println(temp);

    }
}
