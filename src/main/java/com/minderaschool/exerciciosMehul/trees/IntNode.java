package com.minderaschool.exerciciosMehul.trees;

public class IntNode {
    public int value;
    public IntNode left;
    public IntNode right;

    public IntNode(int value, IntNode left, IntNode right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }
}