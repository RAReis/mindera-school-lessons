package com.minderaschool.exerciciosMehul.socialMindera;

import java.util.Scanner;

public class StartMenu {
    private boolean exit = false;
    private void printHeader(){
        System.out.println(" /-------------------------------\\");
        System.out.println("| ============ MENU ============= |");
        System.out.println(" \\-------------------------------/");
        System.out.println();

    }

    private void printMenu(){
        System.out.println("        Choose an option");
        System.out.println("          1 -> Login");
        System.out.println("          2 -> Register");
        System.out.println("          0 -> Exit");
    }

    public void runMenu(){
        printHeader();
        while(!exit){
            printMenu();
            int choice = getInput();
            actionReader(choice);
        }
    }

    private int getInput(){
        Scanner sc = new Scanner(System.in);
        int choice = -1;

        while(choice < 0 || choice > 2){
            try{
                System.out.println("\nEnter your choise: ");
                choice = sc.nextInt();
            }catch(Exception e){
                System.out.println("Choice not available. Try again.");
            }
        }
        return choice;
    }

    private void actionReader(int choice){
        switch (choice){
            case(0):
                exit = true;
                System.out.println("See you next time");
                break;
            case(1):
                //login();
                break;
            case(2):
                //register();
                break;
            default:
                System.out.println("Error in actionReader method");
        }
    }

    public static void main(String[] args) {
        StartMenu menu = new StartMenu();
        menu.runMenu();
    }
}
