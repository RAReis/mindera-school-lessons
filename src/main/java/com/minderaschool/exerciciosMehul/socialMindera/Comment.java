package com.minderaschool.exerciciosMehul.socialMindera;

public class Comment {
    private String username;
    private String text;

    public Comment(String username, String text){
        this.username = username;
        this.text = text;
    }

    public String getUsername() {
        return this.username;
    }

    public String getText() {
        return this.text;
    }
}
