package com.minderaschool.exerciciosMehul.socialMindera;

import java.util.ArrayList;
import java.util.List;

public class Post {
    private String username;
    private String text;
    private List<String> likeList;
    private List<Comment> commentList;

    public Post(String text, String username) {
        this.username = username;
        this.text = text;
        this.likeList = new ArrayList<>();
        this.commentList = new ArrayList<>();
    }

    public void like(String username) {
        boolean exists = false;//Este boolean vai dizer se o "username" já existe ou não na "likeList"
        for (int i = 0; i < this.likeList.size(); i++) {
            if (this.likeList.get(i).equals(username)) {
                exists = true;
                break;
            }
        }
        if (!exists) {
            this.likeList.add(username);
        }
    }

    public String[] getLikes() {
        String[] likeArr = new String[this.likeList.size()];

        for (int i = 0; i < this.likeList.size(); i++) {
            likeArr[i] = this.likeList.get(i);
        }
        return likeArr;
    }

    public void unlike(String username) {
        for (int i = 0; i < this.likeList.size(); i++) {
            if (this.likeList.get(i).equals(username)) {
                this.likeList.remove(username);
                break;
            }
        }
    }

    public void comment(String username, String text) {
        this.commentList.add(new Comment(username, text)); // esta lista só recebe objetos do tipo Comment
    }

    public Comment[] getComments() {
        Comment[] commentArr = new Comment[this.commentList.size()];

        for (int i = 0; i < this.commentList.size(); i++) {
            commentArr[i] = this.commentList.get(i);
        }
        return commentArr;
    }

    public void print() {
        System.out.println("- - -");
        System.out.println(this.text + " - " + this.username);
        System.out.println("- - -");
        System.out.print("Likes: ");
        if (this.likeList.size() > 0) {

            for (String user : this.likeList) {
                System.out.print(user + " ");
            }
        } else {
            System.out.print("No Likes.");
        }
        System.out.println("\n- - -");
        if (this.commentList.size() > 0) {
            for (Comment comment : this.commentList) {
                System.out.println("Comment: " + comment.getText() + " - " + comment.getUsername());
            }
        } else {
            System.out.println("No Comments.");
        }
        System.out.println("- - -");
        System.out.println();
    }
}
