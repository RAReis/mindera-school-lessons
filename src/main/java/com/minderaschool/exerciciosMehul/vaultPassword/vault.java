package com.minderaschool.exerciciosMehul.vaultPassword;

public class vault {
    public static void main(String[] args) {
        int[] code = {0, 0, 0, 0};
        int[] key = {0, 0, 0, 0};

        int and1 = and(code[0], key[0]);
        int and2 = and(code[1], key[1]);
        int and3 = and(code[2], key[2]);
        int and4 = and(code[3], key[3]);

        int or1 = or(code[0], key[0]);
        int or2 = or(code[1], key[1]);
        int or3 = or(code[2], key[2]);
        int or4 = or(code[3], key[3]);


        /** System.out.println( or(
         and(code[1], key[1]),
         not(or(code[1], key[1]))
         ));**/

    }


    static int lock(int[] code, int[] key) {
        return and(
                and(

                        or(
                                and(code[0], key[0]),
                                not(or(code[0], key[0]))
                        ),

                        or(
                                and(code[1], key[1]),
                                not(or(code[1], key[1]))
                        )
                ),
                and(

                        or(
                                and(code[2], key[2]),
                                not(or(code[2], key[2]))
                        ),

                        or(
                                and(code[3], key[3]),
                                not(or(code[3], key[3]))
                        )
                )
        );
    }

    static int and(int i1, int i2) {
        return i1 & i2;
    }

    static int or(int i1, int i2) {
        return i1 | i2;
    }


    static int not(int i) {
        return and(0x01, ~i);
    }
}
