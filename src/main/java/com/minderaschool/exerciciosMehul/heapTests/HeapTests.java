package com.minderaschool.exerciciosMehul.heapTests;

public class HeapTests {
    public static void main(String[] args) {
        boolean isMinHeap = false;
        int[] arr = {0, 3, 4, 5, 10, 7, 15};
        int[] arr1 = {0, 10, 11, 15, 9, 12, 13};
        int[] arr2 = {10, 9, 5, 6, 3, 2};
        System.out.println("Min Heap: " + minHeap(arr));
        System.out.println("Max Heap: " + maxHeap(arr2));

    }


    public static boolean minHeap(int[] arr) {
        for (int i = 1; i < arr.length; i++) {

            if ((arr[(i - 1) / 2]) > arr[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean maxHeap(int[] arr) {
        for (int i = 0; i < arr.length; i++) {

            if ((arr[(i - 1) / 2]) < arr[i]) {
                return false;
            }
        }
        return true;
    }
}
