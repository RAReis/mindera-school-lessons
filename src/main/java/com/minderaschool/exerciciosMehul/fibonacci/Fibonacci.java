package com.minderaschool.exerciciosMehul.fibonacci;

import java.util.Arrays;

public class Fibonacci {
    public static void main(String[] args) {
        //int value = fib(5);
        //System.out.println(value);
        int[] arr = fibSequence(5);
        for (int i = 0; i < arr.length; i++){
            System.out.println(arr[i]);
        }
    }

    public static int fib(int position){
        if(position == 0){
            return 1;
        }

        if(position == 1){
            return 1;
        }

        int value = fib(position-1) + fib(position-2);
        return value;
    }

    public static int[] fibSequence(int length){


        if(length == 1){
            int[] arr = {1};
            return arr;
        }

        if(length == 2){
            int[] arr = {1,1};
            return arr;
        }

        int[] arr = Arrays.copyOf(fibSequence(length-1), length);
        arr[length-1] = arr[length-2] + arr[length-3];
        return arr;
    }
}
