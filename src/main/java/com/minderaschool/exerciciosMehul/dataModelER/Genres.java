package com.minderaschool.exerciciosMehul.dataModelER;

import java.util.List;

public class Genres {
    private String name;
    private List<Game> gameList;

    public Genres(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Game> getGameList() {
        return gameList;
    }

    public void addGame(Game game) {
        this.gameList.add(game);
    }

    public void deleteGame(Game game) {
        this.gameList.remove(game);
    }
}
