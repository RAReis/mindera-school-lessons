package com.minderaschool.exerciciosMehul.dataModelER;

public class Rating {
    int rate;
    int rateCount = 0;

    public Rating(int rate) {
        if(rate < 0 || rate > 5){
            System.out.println("Error: Rate must be from 0 to 5.");
        }
        this.rate = rate;
        rateCount++;
        //this.rate = (this.rate + rate)/rateCount;
    }
}
