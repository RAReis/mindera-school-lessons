package com.minderaschool.exerciciosMehul.dataModelER;

import java.util.List;

public class Game {
    private String name;
    private List<Genres> genres;
    private List<Platforms> platform;
    private List<Comments> comments;

    public Game(String name, List<Genres> genres, List<Platforms> platform) {
        this.name = name;
        this.genres = genres;
        this.platform = platform;

        for (Genres g : genres) {
            g.addGame(this);
        }

        for(int i=0;i<genres.size();i++){
            genres.get(i).addGame(this);
        }

        for (Platforms p : platform) {
            p.addGames(this);
        }
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void addComment(Comments comment) {
        this.comments.add(comment);
    }

    public void removeComent(Comments comment){
        this.comments.remove(comment);
    }

    public void addGenre(Genres genre){
        this.genres.add(genre);
    }

    public void removeGenre(Genres genre){
        this.genres.remove(genre);
    }

    public void addPlatform(Platforms plat){
        this.platform.add(plat);
    }

    public void removePlatform(Platforms plat){
        this.platform.remove(plat);
    }
}
