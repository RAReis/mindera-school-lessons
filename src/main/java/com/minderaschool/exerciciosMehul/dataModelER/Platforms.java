package com.minderaschool.exerciciosMehul.dataModelER;

import java.util.List;

public class Platforms {
    private String name;
    private List<Game> games;
    private List<Comments> comments;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Platforms(String name) {
        this.name = name;
    }

    public List<Game> getGames() {
        return games;
    }

    public void addGames(Game game) {
        this.games.add(game);
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void addComment(Comments comment) {
        this.comments.add(comment);
    }
}
