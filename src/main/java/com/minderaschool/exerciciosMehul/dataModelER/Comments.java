package com.minderaschool.exerciciosMehul.dataModelER;

public class Comments {
    private int user_id;
    private String comment;


    public Comments(int user_id, String comment) {
        this.user_id = user_id;
        this.comment = comment;
    }

    public int getUser_id() {
        return user_id;
    }

}
