package com.minderaschool.exerciciosMehul.dataModelER;

import com.minderaschool.exerciciosMehul.socialMindera.Comment;

import java.util.LinkedList;
import java.util.List;

public class User {
    String id;

    List<Comments> commentsList;

    public User(String id) {
        this.id = id;
        this.commentsList = new LinkedList<>();
    }

    public void addComment(Comments comment){
        this.commentsList.add(comment);
    }

    public void removeComment(Comment comment) {
        this.commentsList.remove(comment);
    }
}
