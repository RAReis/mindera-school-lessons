package com.minderaschool.arrayOredering;

import java.util.Scanner;

public class ArrayOrdering {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n How many numbers you wanto to insert?\n");
        int size = scanner.nextInt();
        int[] array = new int[size];
        System.out.println("Select your numbers: \n");
        int count = 0;
        int[] originArr = new int[size];

        do {
             originArr[count] = scanner.nextInt();
             array[(size-1)-count] = originArr[count];
             count ++;
        } while (count < size);


        System.out.println("Your numbers in reverse order are: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]+ " ");
        }
    }
}
