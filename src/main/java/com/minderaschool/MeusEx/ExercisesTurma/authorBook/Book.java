package com.minderaschool.MeusEx.ExercisesTurma.authorBook;

public class Book {
    private String name;
    private Author author;
    private double price;
    public  Book(String name, Author author, double price){
        this.name = name;
        this.author = author;
        this.price = price;
    }
    public String getName(){
        return this.name;
    }

    public Author getAuthor(){
        return this.author;
    }

    public double getPrice(){
        return this.price;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public void printAuthorData(){
        System.out.println("O autor chama-se " + author.getName() + " é do sexo " + author.getGender()
                + " e tem o e-mail " + author.getEmail());
    }
}
