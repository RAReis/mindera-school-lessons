package com.minderaschool.MeusEx.ExercisesTurma.authorBook;

public class AuthorMain {
    public static void main(String[]args) {

        Author author = new Author("Mindera School", "teste@gmail.com",'m');
        Book book = new Book("Os Maias", author, 21.5d);

        System.out.println("O livro: " + book.getName());
        System.out.println("Preço: " + book.getPrice());
        book.printAuthorData();
    }
}
