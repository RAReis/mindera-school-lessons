package com.minderaschool.MeusEx.ExercisesTurma.authorBook;

public class Author {
    private String name;
    private String email;
    private char gender;
    public Author(String name, String email, char gender){
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName(){
        return this.name;
    }

    public String getEmail(){
        return this.email;
    }

    public String getGender(){
        if(this.gender == 'm' || this.gender == 'M'){
            return "masculino";
        } else if(this.gender == 'f' || this.gender == 'F' ){
            return "feminino";
        } else{
            System.out.println("Error! Gender non existent!");
            return "Non existent gender!";
        }
    }
}
