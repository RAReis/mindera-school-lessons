package com.minderaschool.MeusEx.ExercisesTurma.personStudentTeacher;

public class Teacher extends Person {
    private int numCourses;
    private String[] courses;

    public Teacher(String name, String adress) {
        super(name, adress);
        numCourses = 0;
        courses = new String[5];
    }

    public boolean addCourse(String course) {
        if( numCourses == 5){
            return false;
        }
        for (int i = 0; i < numCourses; i++) {
            if (courses[i].equals(course)) {
                return false;
            }
        }
        courses[numCourses] = course;
        numCourses++;
        return true;
    }

    public boolean removeCourse(String course) {
        if( numCourses == 0){
            return false;
        }
        for(int i = 0; i < numCourses; i++){
            if (courses[i].equals(course)){
                for(int j = i; j < numCourses-1; j++){
                    courses[j] = courses[j+1];
                }
                numCourses--;
                return true;
            }
        }
        return false;
    }

    public void printCourses() {
        for (int i = 0; i < numCourses; i++) {
            System.out.printf("\n %s", courses[i]);
        }
        System.out.println();
    }

    @Override
    public String toString() {
        return "Teacher: " + super.toString();
    }
}
