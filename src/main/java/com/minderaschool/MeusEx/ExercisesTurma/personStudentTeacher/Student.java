package com.minderaschool.MeusEx.ExercisesTurma.personStudentTeacher;

public class Student extends Person {
    private int numCourses;
    private String[] courses;
    private int[] grades;

    public Student(String name, String adress){
        super(name, adress);
        numCourses = 0;
        courses = new String[5];
        grades = new int[5];

    }

    public void addCourseGrade(String course, int grade){
            courses[numCourses] = course;
            grades[numCourses] = grade;
            numCourses++;
    }

    public void printGrades(){
        for(int i = 0; i< numCourses; i++){
            System.out.print("Course: " + this.courses[i] + " grade: " + this.grades[i]);
        }
    }

    public double getAverageGrade(){
        int total = 0;
        for(int i = 0; i < this.numCourses; i++){
            total += this.grades[i];
        }
        return total/this.numCourses;
    }

    @Override
    public String toString(){
        return "Student: "+ super.toString();
    }


}
