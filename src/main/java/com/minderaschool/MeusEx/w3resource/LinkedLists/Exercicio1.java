package com.minderaschool.MeusEx.w3resource.LinkedLists;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

public class Exercicio1 {
    public static void main(String[] args) {
        //1
        LinkedList<String> list = new LinkedList<>();
        list.add("first");
        list.add("second");
        list.add("third");
        list.add("fourth");
        System.out.println(list + "\n");
        //2
        iterate(list);
        System.out.println();
        //3
        iterateFromIndex(list,1);
        System.out.println();
        //4
        iterateReverse(list);
        System.out.println();
        //5
        addAtIndex(list,2, "newThird");
        //6
        addAtFirst(list,"newFirst");
        //7
        addAtLast(list, "Last");
        System.out.println(list);
        System.out.println();
        //8
        LinkedList<String> list2 = new LinkedList<>();
        list2.add("list2-first");
        list2.add("list2-second");

        addFromList(list, list2);
        System.out.println(list);
        System.out.println();
        //16
        //shuffleList(list);
        //System.out.println(list);

        System.out.println(list.pop()); // remove primeiro elemento

    }

    public static void iterate(LinkedList<String> list){
        for (String iterator : list) {
            System.out.println(iterator);
        }
    }

    public static void iterateFromIndex(LinkedList<String> list, int index){
        Iterator start = list.listIterator(index);

        while(start.hasNext()){
            System.out.println(start.next());
        }
    }

    public static void iterateReverse(LinkedList<String> list){
        Iterator reverse = list.descendingIterator();

        while(reverse.hasNext()){
            System.out.println(reverse.next());
        }
    }

    public static void addAtIndex(LinkedList<String> list, int index, String str){
        list.add(index,str);
    }

    public static void addAtFirst(LinkedList<String> list, String str){
        list.addFirst(str);
    }

    public static void addAtLast(LinkedList<String> list, String str){
        list.addLast(str);
    }

    public static void addFromList(LinkedList<String> list, LinkedList<String> list2){
        list.addAll(list2);
    }

    public static void shuffleList(LinkedList<String> list){
        Collections.shuffle(list);
    }
}
