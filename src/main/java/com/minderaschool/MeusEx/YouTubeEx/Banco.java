package com.minderaschool.MeusEx.YouTubeEx;

public class Banco {
    public int numConta;
    protected String tipo;
    private String dono;
    private double saldo;
    private boolean isOpen;

    public void abrirConta(String tipo, String dono) {
        this.setNumConta((int)(Math.random() * 9999));
        this.setTipo(tipo);
        this.setDono(dono);
        this.setOpen(true);
        if (tipo == "CC") {
            this.setSaldo(50);
        } else if (tipo == "CP") {
            this.setSaldo(150);
        } else {
            System.out.println("Tipo de conta não existente. \n Escolha entre CC ( Conta Corrente ) ou " +
                    "CP ( Conta Poupança ).");
        }
    }

    public void fecharConta() {
        if (this.getSaldo() > 0) {
            System.out.printf("A conta tem %.2f saldo disponivél.\n" +
                    "Levante o seu dinheiro antes de encerrar a conta.", this.getSaldo());
        } else if (this.getSaldo() < 0) {
            System.out.printf("O seu saldo é negativo.\n Liquide a sua divida para poder encerrar a conta.");
        } else {
            this.setOpen(false);
            System.out.println("Conta encerrada com sucesso.");
        }
    }

    public void depositar(double valor) {
        if (this.getOpen() == true) {
            this.setSaldo(this.getSaldo() + valor);
        } else {
            System.out.println("ERRO: Conta encerrada.");
        }
    }

    public void levantar(double valor) {
        if (this.getOpen()) {
            if(this.getSaldo() >= valor){
            this.setSaldo(this.getSaldo() - valor);
            } else {
                System.out.printf("ERRO: O saldo disponivél é %.2f .\n", this.getSaldo());
            }
        } else {
            System.out.println("ERRO: Conta encerrada.");
        }
    }

    public void pagarMensalidade() {
        int valorMes;
        if (this.getOpen() == true) {
            if (this.getTipo() == "CC") {
                valorMes = 10;
                if(this.getSaldo() >= valorMes){
                setSaldo(this.getSaldo() - 5);
                    System.out.println("Mensalidade paga com sucesso.");
                } else {
                    System.out.printf("ERRO: O saldo disponivél é %.2f .\n", this.getSaldo());
                }
            } else {
                valorMes = 3;
                if(this.getSaldo() >= valorMes){
                setSaldo(this.getSaldo() - 1);
                    System.out.println("Mensalidade paga com sucesso.");
                } else {
                    System.out.printf("ERRO: O saldo disponivél é %.2f .\n", this.getSaldo());
                }
            }
        } else {
            System.out.println("ERRO: Conta encerrada.");
        }
    }

    public void setNumConta(int numConta) {
        this.numConta = numConta;
    }

    public int getNumConta() {
        return numConta;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setDono(String dono) {
        this.dono = dono;
    }

    public String getDono() {
        return dono;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public boolean getOpen() {
        return isOpen;
    }
}

