package com.minderaschool.MeusEx.YouTubeEx;

public class MainBanco {
    public static void main(String[] args) {
        Banco conta1 = new Banco();

        conta1.abrirConta("CP", "Zé");
        System.out.println(conta1.getSaldo());
        conta1.depositar(55.50);
        System.out.println(conta1.getNumConta());
        conta1.levantar( 1000);
        conta1.levantar(35.5);
        System.out.println(conta1.getSaldo());
        conta1.levantar(10);
        System.out.println(conta1.getSaldo());
        conta1.pagarMensalidade();
        System.out.println(conta1.getSaldo());
    }
}
