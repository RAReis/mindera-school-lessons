package com.minderaschool.exerciciosVicente.threads.test;

import com.minderaschool.exerciciosVicente.threads.utils.Client;

import java.util.Queue;
import java.util.concurrent.Semaphore;

public class Receptionist implements Runnable {

    private Queue<Client> clients;
    private Semaphore semaphore;

    public Receptionist(Queue q, Semaphore semaphore) {
        this.clients = q;
        this.semaphore = semaphore;
    }

    public synchronized void addClient(Client c) {
        System.out.println("[Receptionist] Main gave me client " + c.getId());
        this.clients.add(c);
    }

    private synchronized Client pollClient() {
        Client c = this.clients.poll();
        System.out.println("[Receptionist] Sending client to employee: " + c.getId());
        return c;
    }

    @Override
    public void run() {
        while (true) {

            if (!clients.isEmpty()) {

                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                clients.add(pollClient());

                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //semaphore.release();
            }
        }
    }
}
