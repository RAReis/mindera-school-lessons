package com.minderaschool.exerciciosVicente.threads.test;

import com.minderaschool.exerciciosVicente.threads.utils.Client;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Main {

    public static void main(String[] args) {
        Random rand = new Random();
        Semaphore mutex = new Semaphore(0);
        Queue<Client> clients = new LinkedList<>();

        Employee e = new Employee(clients, mutex);
        Receptionist r = new Receptionist(clients, mutex);

        new Thread(() -> {
            int i = 0;
            int numOfClients = rand.nextInt((10 - 1)+1);
            System.out.println("[Main] random number " + numOfClients);
            while (numOfClients < 7) {
                numOfClients = rand.nextInt((10 - 1)+1);
                //System.out.println("[Main] random number " + numOfClients);

                i++;

                r.addClient(new Client(i));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }


            }
            mutex.release();
        }).start();

        new Thread(e).start();
        new Thread(r).start();

    }
}
