package com.minderaschool.exerciciosVicente.threads.test;

import com.minderaschool.exerciciosVicente.threads.utils.Client;

import java.util.Queue;
import java.util.concurrent.Semaphore;

public class Employee implements Runnable {

    private Queue<Client> clients;
    private Semaphore semaphore;

    public Employee(Queue q, Semaphore semaphore) {
        this.clients = q;
        this.semaphore = semaphore;
    }

    public void addClient(Client c) {
        System.out.println("[Employee] Receptionist sent me client " + c.getId());
        clients.add(c);
    }

    public synchronized Client pollClient() {
        Client c = clients.poll();
        System.out.println("[Employee] Sitting client " + c.getId());
        return c;
    }

    @Override
    public void run() {
        while(true) {

            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(!clients.isEmpty()) {
                pollClient();
                try {
                    Thread.sleep(1700);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                semaphore.release();
            }
        }
    }
}
