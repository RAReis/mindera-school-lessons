package com.minderaschool.exerciciosVicente.threads;

public class RestaurantQueue {


    public static void main(String[] args) {

        TableEmployee employee = new TableEmployee();
        Receptionist receptionist = new Receptionist(employee.emploQueue, employee.empSemaphore);
        ClientGenerator generator = new ClientGenerator(receptionist.receptQueue, receptionist.recSemaphore);

        try {
            employee.start();
            receptionist.start();
            generator.start();
            generator.join();
            employee.join();
            receptionist.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //generator.run();
        //employee.run();
        //receptionist.run();
    }
}
