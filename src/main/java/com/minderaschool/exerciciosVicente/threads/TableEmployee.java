package com.minderaschool.exerciciosVicente.threads;

import com.minderaschool.exerciciosVicente.threads.utils.Client;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

public class TableEmployee extends Thread {
    public Queue<Client> emploQueue;
    public Semaphore empSemaphore;

    public TableEmployee() {
        emploQueue = new ConcurrentLinkedQueue<>();
        empSemaphore = new Semaphore(0);
    }

    @Override
    public void run() {
        //while (empSemaphore.availablePermits() > 0) {

        while (true) {
            try {
                empSemaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //while (!emploQueue.isEmpty()) {

                synchronized (emploQueue) {
                    Client c = emploQueue.poll();
                    System.out.println(Thread.currentThread() + " Removed client from EmployeeQueue " + c.getId());
                }
            //}

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //}
        }
    }
}
