package com.minderaschool.exerciciosVicente.threads;

import com.minderaschool.exerciciosVicente.threads.utils.Client;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

// Receive clients in a Queue and send them to TableEmployee
public class Receptionist extends Thread {
    public Queue<Client> receptQueue;
    public Queue<Client> employeeQueue;
    private Client c;
    public Semaphore recSemaphore;
    Semaphore empSemaphore;

    public Receptionist(Queue<Client> employeeQueue, Semaphore semaphore) {
        receptQueue = new ConcurrentLinkedQueue<>();
        recSemaphore = new Semaphore(0);
        this.employeeQueue = employeeQueue;
        this.empSemaphore = semaphore;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread() + " Starting ReceptionistQueue ");

        while (true) {

            try {
                recSemaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread() + " Acquired ReceptionistQueue ");


            //while (!receptQueue.isEmpty()) {

            synchronized (receptQueue) {
                c = receptQueue.poll();
                System.out.println(Thread.currentThread() + " Removed client from ReceptionistQueue " + c.getId());
            }

            synchronized (employeeQueue) {
                employeeQueue.add(c);
                System.out.println(Thread.currentThread() + " Added client to EmployeeQueue " + c.getId());
            }


            empSemaphore.release(1);

            try {
                Thread.sleep(100);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //if(empSemaphore.availablePermits() == 0) {

            //}
            //}
            //recSemaphore.release(1);
        }


        //}
    }
}
