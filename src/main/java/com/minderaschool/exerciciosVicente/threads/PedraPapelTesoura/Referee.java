package com.minderaschool.exerciciosVicente.threads.PedraPapelTesoura;

import java.util.Queue;

public class Referee implements Runnable {

    int scorePlayer1 = 0;
    int scorePlayer2 = 0;
    Queue list1;
    Queue list2;
    Integer play1;
    Integer play2;


    public Referee(Queue list1, Queue list2) {
        this.list1 = list1;
        this.list2 = list2;
    }

    // 0 -> Rock | 1 -> Paper | 2 -> Scissor
    public void getResult() {

        play1 = (Integer) list1.remove();
        play2 = (Integer) list2.remove();

        if (play1.equals(play2)) {
            System.out.println("Draw Game");
        } else if (play1.equals(0) && play2.equals(1)) {
            System.out.println("Player 2 wins!");
            scorePlayer2++;
        } else if (play1.equals(1) && play2.equals(2)) {
            System.out.println("Player 2 Wins!");
            scorePlayer2++;
        } else if (play1.equals(2) && play2.equals(0)) {
            System.out.println("Player 2 Wins!");
            scorePlayer2++;
        } else {
            System.out.println("Player 1 Wins!");
            scorePlayer1++;
        }
    }

    @Override
    public void run() {

        if(Semaphores.semaphoreFinished.availablePermits() == 0) {
            //System.out.println("[REFEREE]" + Thread.currentThread().getName() + " ");
            Semaphores.semaphoreP1.release();
            Semaphores.semaphoreP2.release();

            try {
                Semaphores.semaphoreRef.acquire(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            getResult();

            Semaphores.semaphoreNextRound.release();
        }

    }

    public int getScorePlayer1() {
        return scorePlayer1;
    }

    public int getScorePlayer2() {
        return scorePlayer2;
    }
}