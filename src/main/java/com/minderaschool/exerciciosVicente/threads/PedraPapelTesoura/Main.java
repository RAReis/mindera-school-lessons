package com.minderaschool.exerciciosVicente.threads.PedraPapelTesoura;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        final int finalScore = 3;
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(3);

        Queue<Integer> listP1 = new PriorityQueue<>();
        Queue<Integer> listP2 = new PriorityQueue<>();

        Player p1 = new Player(1,listP1);
        Player p2 = new Player(2, listP2);
        Referee ref = new Referee(listP1, listP2);

        do {
            scheduledExecutorService.schedule(p1, 1000, TimeUnit.MILLISECONDS);
            scheduledExecutorService.schedule(p2, 1000, TimeUnit.MILLISECONDS);
            scheduledExecutorService.schedule(ref, 1000, TimeUnit.MILLISECONDS);

            if (ref.getScorePlayer1() >= finalScore || ref.getScorePlayer2() >= finalScore) {
                Semaphores.semaphoreFinished.release();
            } else {
                Semaphores.semaphoreNextRound.acquire();
            }
        } while (Semaphores.semaphoreFinished.availablePermits() == 0);

        //while (ref.scorePlayer2 < 5 && ref.scorePlayer1 < 5); // sem volatile, problemas de visibilidade entre threads
        //while (ref.scorePlayer2 < 5 && ref.scorePlayer1 < 5); // com volatile, problemas de visibilidade entre threads


        scheduledExecutorService.shutdown();

        int scorePlayer1 = ref.getScorePlayer1();
        int scorePlayer2 = ref.getScorePlayer2();
        if (scorePlayer1 > scorePlayer2) {
            printMatchWinner(scorePlayer1, scorePlayer2, 1);
        } else {
            printMatchWinner(scorePlayer1, scorePlayer2, 2);
        }
    }

    private static void printMatchWinner(int scorePlayer1, int scorePlayer2, int winner) {
        System.out.printf("\n\nPlayer 1: [ %d Points ] || Player 2: [ %d Points ]\n", scorePlayer1, scorePlayer2);
        System.out.printf("\nPlayer %d WINS THE MATCH!!\n", winner);
    }
}
