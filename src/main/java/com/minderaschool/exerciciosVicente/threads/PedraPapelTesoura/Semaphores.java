package com.minderaschool.exerciciosVicente.threads.PedraPapelTesoura;

import java.util.concurrent.Semaphore;

public class Semaphores {
    public static final Semaphore semaphoreRef = new Semaphore(0);
    public static final Semaphore semaphoreP1 = new Semaphore(0);
    public static final Semaphore semaphoreP2 = new Semaphore(0);
    public static final Semaphore semaphoreNextRound = new Semaphore(0);
    public static final Semaphore semaphoreFinished = new Semaphore(0);
}
