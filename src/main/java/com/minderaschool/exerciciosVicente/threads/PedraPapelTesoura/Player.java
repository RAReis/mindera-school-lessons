package com.minderaschool.exerciciosVicente.threads.PedraPapelTesoura;

import java.util.Queue;
import java.util.Random;

public class Player implements Runnable {
    Random rand = new Random();
    Integer play = -1;
    Queue list;
    int name;

    public Player(int name, Queue list) {
        this.name = name;
        this.list = list;
    }


    public void play() throws InterruptedException {


        switch (name) {
            case 1:
                Semaphores.semaphoreP1.acquire();
                break;
            case 2:
                Semaphores.semaphoreP2.acquire();
                break;
        }

        play = rand.nextInt((3 - 1) + 1);
        list.add(play);

        if (play == 0) {
            System.out.println("Player " + name + " -> ROCK");
        } else if (play == 1) {
            System.out.println("Player " + name + " -> PAPER");
        } else {
            System.out.println("Player " + name + " -> SCISSOR");
        }

        Semaphores.semaphoreRef.release();
    }

    @Override
    public void run() {

        //System.out.println("[ " + this.name + " ]" + Thread.currentThread().getName());
        if (Semaphores.semaphoreFinished.availablePermits() == 0) {
            try {
                play();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}

