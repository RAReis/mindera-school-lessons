package com.minderaschool.exerciciosVicente.threads;

import com.minderaschool.exerciciosVicente.threads.utils.Client;

import java.util.Queue;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class ClientGenerator extends Thread {

    private Queue<Client> receptionistQueue;
    Random rand = new Random();
    int maxNumberOfClients = 5;
    Semaphore recSemaphore;

    public ClientGenerator(Queue<Client> receptionistQueue, Semaphore semaphore) {
        this.receptionistQueue = receptionistQueue;
        this.recSemaphore = semaphore;
    }

    public void generate(int numberOfClients) {
        System.out.println(Thread.currentThread() + " Going to generate " + numberOfClients);

        for (int i = 0; i < numberOfClients; i++) {

            synchronized (receptionistQueue) {
                System.out.println(Thread.currentThread() + " Adding");
                receptionistQueue.add(new Client(i));
                System.out.println(Thread.currentThread() + " Added client to ReceptionistQueue");

                recSemaphore.release(1);

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void run() {
        generate(5); //rand.nextInt(maxNumberOfClients)

    }
}
