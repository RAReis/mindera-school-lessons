package com.minderaschool.exerciciosVicente.Trees;

public class BinaryTree {
    public BNode root;


    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();

        bt.addBNode(50, "FunFokinTastic");
        bt.addBNode(25, "O_Magnífico");
        bt.addBNode(15, "Um Nome Qualquer");
        bt.addBNode(30, "Trinta");
        bt.addBNode(75, "Setenta e Cinco");
        bt.addBNode(60, "Sessenta");
        bt.addBNode(85, "Ointenta e Cinco");

        bt.inOrderIterator(bt.root);
        //bt.preOrderIterator(bt.root);
        //bt.postOrderIterator(bt.root);
        System.out.println("\nSerch for specific key:");
        System.out.println(bt.findBNode(25).printNode());
        System.out.println();
        System.out.println("Remove Key 30");
        bt.deleteNode(30);
        bt.inOrderIterator(bt.root);
    }

    public BNode findBNode(int key) {
        BNode iteratorNode = root;
        while (iteratorNode.key != key) {
            if (key < iteratorNode.key) {
                iteratorNode = iteratorNode.leftChild;
            } else {
                iteratorNode = iteratorNode.rightChild;
            }

            if (iteratorNode == null) {
                return null;
            }
        }
        return iteratorNode;
    }

    public void postOrderIterator(BNode iteratorNode) {

        if (iteratorNode != null) {

            postOrderIterator(iteratorNode.leftChild);
            postOrderIterator(iteratorNode.rightChild);
            System.out.println(iteratorNode.printNode());
        }
    }

    public void preOrderIterator(BNode iteratorNode) {

        if (iteratorNode != null) {

            System.out.println(iteratorNode.printNode());
            preOrderIterator(iteratorNode.leftChild);
            preOrderIterator(iteratorNode.rightChild);
        }
    }

    public void inOrderIterator(BNode iteratorNode) {
        if (iteratorNode != null) {

            inOrderIterator(iteratorNode.leftChild);
            System.out.println(iteratorNode.printNode());
            inOrderIterator(iteratorNode.rightChild);
        }
    }

    public void addBNode(int key, String name) {
        BNode newNode = new BNode(key, name);

        if (root == null) {
            root = newNode;
        } else {
            BNode iteratorNode = root;
            BNode parent;

            while (true) {
                parent = iteratorNode;

                if (key < iteratorNode.key) {
                    iteratorNode = iteratorNode.leftChild;
                    if (iteratorNode == null) {
                        parent.leftChild = newNode;
                        return;
                    }
                } else {
                    iteratorNode = iteratorNode.rightChild;
                    if (iteratorNode == null) {
                        parent.rightChild = newNode;
                        return;
                    }
                }
            }
        }
    }

    public boolean deleteNode(int key) {
        BNode iteratorNode = root;
        BNode parent = root;

        boolean isItALeftChild = true; //Este boolean vai ser usado para sabermos para que lado procurar o node left/right

        if(root == null){ // tree is empty
            return false;
        }
        while (iteratorNode.key != key) {
            parent = iteratorNode;

            if (key < iteratorNode.key) {

                isItALeftChild = true;
                iteratorNode = iteratorNode.leftChild;

            } else {

                isItALeftChild = false;
                iteratorNode = iteratorNode.rightChild;
            }

            if (iteratorNode == null) {
                return false;
            }
        }

        if (iteratorNode.leftChild == null && iteratorNode.rightChild == null) { // Se o node nao tiver childs

            if (iteratorNode == root) {   // Apaga-mos o node se este for a root e nao tiver childs
                root = null;
            } else if (isItALeftChild) {  // Apaga-mos o node se este for um leftChild do parent e nao tiver childs
                parent.leftChild = null;
            } else {
                parent.rightChild = null; // Apaga-mos o node se este for um rightChild do parent e nao tiver childNodes
            }

        } else if (iteratorNode.rightChild == null) { // Se nao tiver rightChild

            if (iteratorNode == root) {
                root = iteratorNode.leftChild;
            } else if (isItALeftChild) {
                parent.leftChild = iteratorNode.leftChild;
            } else {
                parent.rightChild = iteratorNode.leftChild;
            }

        } else if (iteratorNode.leftChild == null) { // Se nao tiver leftChild

            if (iteratorNode == root) {
                root = iteratorNode.rightChild;
            } else if (isItALeftChild){
                  parent.leftChild = iteratorNode.rightChild;
            } else {
                parent.rightChild = iteratorNode.rightChild;
            }

        } else { // Se tiver dois childNodes vamos ter que descobrir qual o node que vai substituir o node apagado
            
             BNode replaceNode = getReplaceNode(iteratorNode);

             if(iteratorNode == root){
                 root = replaceNode;
             } else if(isItALeftChild){
                 parent.leftChild = replaceNode;
             } else {
                 parent.rightChild = replaceNode;
             }

             replaceNode.leftChild = iteratorNode.leftChild;
        }

        return true;
    }

    public BNode getReplaceNode(BNode replaced_Node){

        BNode replaceParent = replaced_Node;
        BNode replaceNode = replaced_Node;

        BNode iteratorNode = replaced_Node;

        while(iteratorNode != null){
            replaceParent = replaced_Node.rightChild;
            replaceNode = iteratorNode;
            iteratorNode = iteratorNode.leftChild;
        }

        if(replaceNode != replaced_Node.rightChild){

            replaceParent.leftChild = replaceNode.rightChild;
            replaceNode.rightChild = replaced_Node.rightChild;

        }
        return replaceNode;
    }

    public BinaryTree() {
        this.root = root;
    }
}

/**
 * //==========================================================//
 **/

class BNode {
    int key;
    String name;

    BNode leftChild;
    BNode rightChild;

    public BNode(int key, String name) {
        this.key = key;
        this.name = name;
    }

    public String printNode() {
        return name + " has a key " + key;
    }
}


