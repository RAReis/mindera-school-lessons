package com.minderaschool.exerciciosVicente.Trees.SimpleTree;

public class Node {
    private Node left;
    private Node right;
    private char element;
    private int num;

    public Node(char element) {
        this.element = element;
        left = right = null;
    }

    public Node(int num){
        this.num = num;
        left = right = null;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public char getElement() {
        return element;
    }

    public int getNum() {
        return num;
    }
}
