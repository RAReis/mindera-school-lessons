package com.minderaschool.exerciciosVicente.Trees.SimpleTree;

public class SimpleTree {
    private Node root;

    public SimpleTree() {  // iniciar tree vazia
        this.root = null;
    }

    public SimpleTree(Node root) { // iniciar tree com um valor
        this.root = root;
    }

    public void insertRoot(char element) {
        this.root = new Node(element);
    }

    public Node getRoot() {
        return root;
    }

    public Node insertLeft(Node node, char element) {
        if (node.getLeft() == null) {
            node.setLeft(new Node(element));
        } else {
            insertLeft(node.getLeft(), element);
        }
        return node.getLeft();
    }

    public Node insertRight(Node node, char element) {
        if (node.getRight() == null) {
            node.setRight(new Node(element));
        } else {
            insertRight(node.getRight(), element);
        }
        return node.getRight();
    }

    public void preOrder(Node node) {

        System.out.print(node.getElement() + " ");

        if (node.getLeft() != null) {
            preOrder(node.getLeft());
        }

        if (node.getRight() != null) {
            preOrder(node.getRight());
        }
    }

    public void inOrder(Node node) {

        if (node.getLeft() != null) {
            inOrder(node.getLeft());
        }

        System.out.print(node.getElement() + " ");

        if (node.getRight() != null) {
            inOrder(node.getRight());
        }

    }

    public void postOrder(Node node){

        if (node.getLeft() != null) {
            postOrder(node.getLeft());
        }

        if (node.getRight() != null) {
            postOrder(node.getRight());
        }

        System.out.print(node.getElement() + " ");

    }
}
