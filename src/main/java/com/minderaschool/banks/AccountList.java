//AccountList:
// - accounts : Account[]
// + Add(Account : Account) : int
// + count() : int;
// + get(int i) : Account;
// + remove (int i);
// + getAll() : Account [];

package com.minderaschool.banks;

public class AccountList {
    private Account[] accounts = new Account[10];
    private int count;

    public int add(Account account){

        if(count==accounts.length){
            Account[] temp = accounts;
            accounts = new Account[accounts.length*2];
            for(int i=0; i<temp.length; i++){
                accounts[i] = temp[i];
            }
        }
        accounts[count]=account;
        count++;
        return count;
    }
    public Account get(int i){
        return accounts[i];
    }
    public Account[] getAll(){
        return accounts;
    }
    public void remove(int index){
        for(int i=index; i<count; i++){
            accounts[i]=accounts[i+1]; //começando no index desejado movemos as accounts um espaço para tras
        }
        accounts[count-1]=null; //transforma-mos o ultimo espaço do array "accounts" em null
        count--; //retiramos 1 a "count" pk o ultimo espaço ficou vazio
    }
}
