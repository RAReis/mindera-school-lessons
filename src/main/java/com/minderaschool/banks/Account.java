package com.minderaschool.banks;

// Enquanto o objeto "Account" nao for inicializado ou chamado, ele nao tem valores guardados ou iniciados.
public class Account {
    private double balance; //0.0

    public Account(double x){
        this.balance = x;

    }

    public void withdraw(double amount){
        this.balance -= amount;

    }

    public void deposit(double amount){
        this.balance += amount;
    }

    public void transfer(double amount, Account dest){
        this.withdraw(amount); // "this." vai apontar para o objecto Account que está a chamar o método "transfer()"
        dest.deposit(amount);
    }

    public double getBalance() {
        return this.balance;

    }

}
