package com.minderaschool.exercicioaGabriel.myLinkedList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MyLinkedListTest {
    private MyLinkedList linkedList;

    @BeforeEach
    void setUp() {
        linkedList = new MyLinkedList();
    }

    @Test
    void whenListIsEmptyGetElementThrowsException() {
        try {
            linkedList.get(0);
            assert(false);
        }catch(ArrayIndexOutOfBoundsException ex) {
            assert(true);
        }
    }

    @Test
    void getIndex0() {
        int index = 0;
        linkedList.addAtHead(1);
        assert(linkedList.getHead().getValue() == linkedList.get(index));

    }

    @Test
    void addAtHeadNull() {

        assert(linkedList.getHead() == null);
        int value = 5;
        linkedList.addAtHead(value);
        assert(linkedList.getHead().getValue() == value);
    }

    @Test
    void addAtHead() {

        int val1 = 5;
        int val2 = 3;
        linkedList.addAtHead(val1);
        linkedList.addAtHead(val2);

        assert(linkedList.getHead().getValue() == val2);
    }

    @Test
    void addAtTail() {
        linkedList.addAtHead(1);
        linkedList.addAtTail(4);
        ///////////////////////////////////assert(linkedList.getTail() == 4);
    }

    @Test
    void addAtIndexZeroWhenListIsEmpty() {
        linkedList.addAtIndex(0, 1);
        assert(linkedList.get(0) == 1);
    }

    @Test
    void addAtIndexOneWhenListIsEmptyThrowsExceptioon() {
        // prepare test initial conditions
        // list is empty
        MyLinkedList someLinkedList = new MyLinkedList();

        try {
            // execute test scenario
            someLinkedList.addAtIndex(1, 1);
            // fail test if didn't throw execption
            assert(false);
        } catch (ArrayIndexOutOfBoundsException e) {
            // pass test because exception was thrown
            assert(true);
        }
    }

    @Test
    void deleteAtIndex() {
    }
}