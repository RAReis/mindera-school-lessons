package com.minderaschool.exercicioaGabriel.threads;

import org.junit.Test;

public class FibonacciTest {

    @Test
    public void firstTest() throws InterruptedException {
        Fibonacci fibonacci = new Fibonacci();


        System.out.println(fibonacci.fibonacci(11L));
    }

    @Test
    public void performance() throws InterruptedException {
        Fibonacci sut = new Fibonacci();

        int times = 30;
        long sum = 0;
        long min = Long.MAX_VALUE;
        long max = Long.MIN_VALUE;
        for (int i = 0; i < times; i++) {
            long n = (long) i;
            long start = System.nanoTime();
            long fib = sut.fibonacci(n);
            long finish = System.nanoTime()-start;
            sum += finish;
            if (finish > max) {
                max = finish;
            }
            if (finish < min) {
                min = finish;
            }
            System.out.printf("\n%4d - %d ns", fib, finish);
        }
        System.out.printf("\n\nMin: %d ns", min);
        System.out.printf("\nAvg: %d ns", (sum/times));
        System.out.printf("\nMax: %d ns\n", max);
    }
}