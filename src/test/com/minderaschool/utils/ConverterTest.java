package com.minderaschool.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConverterTest {

    @Test
    void romanToDecimalShouldConvert1() {
        assertEquals(1, Converter.romanToDecimal("I"));
    }

    @Test
    void romanToDecimalShouldConvert5() {
        assertEquals(5, Converter.romanToDecimal("V"));
    }

    @Test
    void romanToDecimalShouldConvert4() {
        assertEquals(4, Converter.romanToDecimal("IV"));
    }

    @Test
    void romanToDecimalShouldConvert1556() {
        assertEquals(1556, Converter.romanToDecimal("MDLVI"));
    }
}