package com.minderaschool.exerciciosMehul.vaultPassword;

import org.junit.jupiter.api.Test;

import static com.minderaschool.exerciciosMehul.vaultPassword.vault.*;

class vaultTest {
    int[] code = {0,0,1,0};
    int[] key = {0,0,1,0};

    @Test
    void firstLayer(){
        int[] code = {0,0,1,0};
        int[] key = {0,0,1,0};

        assert(1==lock(code,key));
    }

    @Test
    void second() {
        int[] code = {1,0,1,0};
        int[] key = {0,0,1,0};
        System.out.println( or(
                and(code[1], key[1]),
                not(or(code[1], key[1]))
        ));

        assert(0==0);
    }
}