package com.minderaschool.exerciciosVicente.Trees;

import com.minderaschool.exerciciosVicente.Trees.SimpleTree.Node;
import com.minderaschool.exerciciosVicente.Trees.SimpleTree.SimpleTree;
import org.junit.jupiter.api.Test;

class SimpleTreeTest {


    @Test
    void test(){
        SimpleTree st = new SimpleTree(); // null

        st.insertRoot('a');

        Node nodeB = st.insertLeft(st.getRoot(), 'b');
        st.insertRight(nodeB, 'c');

        Node nodeC = st.insertRight(st.getRoot(), 'd');
        st.insertLeft(nodeC, 'e');
        st.insertRight(nodeC, 'f');

        st.preOrder(st.getRoot());
        System.out.println();
        st.inOrder(st.getRoot());
        System.out.println();
        st.postOrder(st.getRoot());
    }

    @Test
    void testBinary(){
        BinaryTree bt = new BinaryTree();

    }
}