package com.minderaschool.exerciciosVasco;

import org.junit.Test;

import java.nio.file.NoSuchFileException;
import java.util.Collection;


//import static com.minderaschool.exerciciosVasco.MetricParser.readAndParseMetrics;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class MetricParserTest {


    @Test(expected = NoSuchFileException.class)
    public void fileDoesntExist() throws Exception {
       MetricParser.readAndParseMetrics("thisissomefilethatihopeitdoesntexistsanywherearoudherepleasedeleteifsuchfileexists.txt");
    }

    @Test
    public void testReadAndParseMetrics() throws Exception {
        Collection<Metric> metrics = MetricParser.readAndParseMetrics("input-simple.txt");

        assertEquals("Must have 10 metrics", 10, metrics.size());
        for (Metric metric : metrics) {
            if (!(metric instanceof MetricCounter || metric instanceof MetricTimer || metric instanceof MetricGauge)) {
                fail("Metric is none of timer, gauge or counter");
            }
        }
    }
}