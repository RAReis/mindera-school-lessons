package andrereis;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class NotQuiteLispTest {

    @Test
    public void solveInstructions() {
        String str = "(())";
        int floor = NotQuiteLisp.solveInstructions(str);

        Assert.assertEquals("Landed on floor", 0, floor);
    }

    @Test
    public void expectedFloor3() {
        String str = "(()(()(";
        int floor = NotQuiteLisp.solveInstructions(str);

        Assert.assertEquals("Landed on floor", 3, floor);
    }
}