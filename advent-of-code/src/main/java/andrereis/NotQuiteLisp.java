package andrereis;

public class NotQuiteLisp {
    public static int solveInstructions(String instructions){
        int currentFloor = 0;
        for(int i = 0; i < instructions.length();i++){
            switch(instructions.charAt(i)){
                case '(' :
                    currentFloor++;
                    break;
                case ')' :
                    currentFloor--;
                    break;
                default :
                    System.out.println("Character not valid");
                    break;
            }
        }
        return currentFloor;
    }
}
